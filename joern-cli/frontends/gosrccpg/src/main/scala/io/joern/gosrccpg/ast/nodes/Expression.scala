package io.joern.gosrccpg.ast.nodes

abstract class Expression extends Node("Expression")

class Identifier extends Expression {
  var namePosition: Int = 0
  var name: Option[String] = None
}

class CallExpression extends Expression {
  var function: Option[Expression] = None
  var lparen: Int = 0
  var rparen: Int = 0
  var args: List[Expression] = List()
  var ellipsis: Int = 0
}
