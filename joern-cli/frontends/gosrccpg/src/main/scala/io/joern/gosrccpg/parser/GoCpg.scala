package io.joern.gosrccpg.parser

import io.joern.gosrccpg.Config
import io.joern.gosrccpg.Frontend.defaultConfig
import io.joern.x2cpg.{X2Cpg, X2CpgFrontend}
import io.shiftleft.codepropertygraph.Cpg

import java.io.File
import scala.util.Try

class GoCpg extends X2CpgFrontend[Config] {

  override def createCpg(config: Config): Try[Cpg] = {
    val inputFile = new File(config.inputPath)
    if (!inputFile.isDirectory && !inputFile.isFile) {
      throw new IllegalArgumentException(s"$inputFile is not a valid directory or file.")
    }

    X2Cpg.withNewEmptyCpg(config.outputPath, config) { (cpg, _) =>
      better.files.File.usingTemporaryDirectory("gosrccpg_tmp") { tempWorkingDir =>

      }
    }
  }

}
