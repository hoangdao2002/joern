package io.joern.gosrccpg.ast.nodes

abstract class Node(val nodeType: String) {
  var start: Int = 0
  var end: Int = 0
  val children: List[Node] = List()
}
