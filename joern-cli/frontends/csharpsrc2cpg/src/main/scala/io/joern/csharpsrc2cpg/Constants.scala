package io.joern.csharpsrc2cpg

object Constants {
  val This: String = "this"
}

object CSharpOperators {
  val throws: String  = "<operators>.throw"
  val unknown: String = "<operators>.unknown"
}
